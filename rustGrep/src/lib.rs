use std::error::Error;
use std::fs;

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    for line in search(&config.query, &contents) {
        println!("{}", line);
    }

    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }
    if results.is_empty() {
        println!("Nothing was found!.");
    }
    results
}

pub struct Config {
    query: String,
    filename: String,
}

impl Config {
    pub fn new(arguments: &[String]) -> Result<Config, &'static str> {
        if (arguments.len() < 3) {
            return Err("Not enough arguments.");
        }
        let query = arguments[1].clone();
        let filename = arguments[2].clone();

        Ok(Config { query, filename })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "fast";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }
}
